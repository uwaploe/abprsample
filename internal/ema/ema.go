// Ema implements an exponential moving average filter
package ema

import (
	"sync"
)

type Ema struct {
	alpha, beta float64
	mu          *sync.RWMutex
	state       float64
	initialized bool
}

func New(alpha float64) *Ema {
	f := &Ema{}
	f.alpha = alpha
	f.beta = 1. - alpha
	f.mu = &sync.RWMutex{}
	return f
}

// Update takes a new input value and returns a new filtered value
func (f *Ema) Update(x float64) float64 {
	if !f.initialized {
		f.state = x
		f.initialized = true
	} else {
		f.mu.Lock()
		f.state = f.alpha*x + f.beta*f.state
		f.mu.Unlock()
	}
	return f.state
}

func (f *Ema) State() float64 {
	return f.state
}
