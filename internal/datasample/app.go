package datasample

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/abprsample/internal/gpio"
	"bitbucket.org/uwaploe/abprsample/internal/modem"
	"bitbucket.org/uwaploe/abprsample/internal/pulse"
	"bitbucket.org/uwaploe/abprsample/internal/sci"
	"github.com/coreos/go-systemd/v22/dbus"
)

const Usage = `Usage: datasample [options]

Run the ABPR data sampling process.

`

type appEnv struct {
	fl          *flag.FlagSet
	sciopts     []sci.Option
	mopts       []modem.Option
	dataDir     string
	deGpio      int
	comGpio     int
	prGpio      int
	wdGpio      int
	wdPeriod    time.Duration
	wdWidth     time.Duration
	prWarmup    time.Duration
	prService   string
	dataService string
	minWait     time.Duration
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("rdpr", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	prdev := fl.String("dev", "/dev/ttyAPP1", "pressure sensor serial device")
	intTime := fl.Duration("int-time", 3500*time.Millisecond, "pressure integration time")
	amdev := fl.String("modem", "/dev/ttyAPP3", "acoustic modem serial device")
	fl.IntVar(&app.deGpio, "de-gpio", 55, "GPIO line for modem Device Enable")
	fl.IntVar(&app.comGpio, "com-gpio", 53, "GPIO to enable modem communications")
	fl.IntVar(&app.prGpio, "pr-gpio", 49, "GPIO to switch pressure sensor power")
	fl.IntVar(&app.wdGpio, "wd-gpio", 52, "GPIO to signal watchdog")
	fl.DurationVar(&app.wdPeriod, "wd-period", time.Second*60,
		"watchdog pulse period")
	fl.DurationVar(&app.wdWidth, "wd-width", time.Millisecond*500,
		"watchdog pulse width")
	fl.DurationVar(&app.prWarmup, "pr-warmup", time.Millisecond*250,
		"pressure sensor warm-up time")
	fl.StringVar(&app.prService, "svc", "rdpr.timer",
		"Systemd timer service to sample pressure")
	fl.StringVar(&app.dataService, "data-svc", "datasvc.service",
		"Systemd service to manage data transfer")
	fl.DurationVar(&app.minWait, "wait", 0, "minimum time to wait for modem DE")
	fl.StringVar(&app.dataDir, "data", "/data/ABPR", "data directory")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	app.sciopts = []sci.Option{
		sci.PressureDevice(*prdev),
		sci.PressureIntTime(*intTime),
		sci.NoTilt(),
	}

	app.mopts = []modem.Option{
		modem.ModemDevice(*amdev),
	}

	return nil
}

func modemSleep(ctx context.Context, opts []modem.Option) {
	m, err := modem.New(opts...)
	if err != nil {
		return
	}
	m.SendCmds(ctx, "@IdleTimer=00:00:40", "cfg store")
	m.Lowpower(ctx)
	m.Close()
}

func modemKeepAwake(ctx context.Context, opts []modem.Option) {
	m, err := modem.New(opts...)
	if err != nil {
		return
	}
	m.SendCmds(ctx, "@IdleTimer=00:05:00", "cfg store")
	m.Lowpower(ctx)
	m.Close()
}

func (app *appEnv) acqSample() error {
	samp, err := sci.New(app.sciopts...)
	if err != nil {
		return err
	}
	defer samp.Close()

	if err := os.MkdirAll(app.dataDir, 0755); err != nil {
		return err
	}

	store := abprdata.NewDataStore(app.dataDir)
	defer store.Close()

	ds, err := samp.Sample()
	if err != nil {
		return err
	}
	rec := abprdata.NewRecord(ds.T, ds)

	return store.AddRecord(rec)
}

func (app *appEnv) checkModem(ctx0 context.Context, g *pulse.Generator) error {
	p := gpio.NewPin(app.deGpio)
	if err := p.In(); err != nil {
		return err
	}

	val, err := p.Read()
	if err != nil {
		return err
	}

	// When the modem DE is asserted, the GPIO line is pulled low
	if val == gpio.High {
		// Wait a bit for a falling edge
		if err := p.WaitForEdge(gpio.Falling, app.minWait); err != nil {
			fmt.Println("DE not asserted, exiting")
			return nil
		}
	}

	comEnable := gpio.NewPin(app.comGpio)
	comEnable.Out()
	comEnable.Write(gpio.High)
	defer comEnable.Write(gpio.Low)

	ctx, cancel := context.WithCancel(ctx0)
	defer cancel()

	conn, err := dbus.NewSystemdConnection()
	if err != nil {
		return err
	}
	defer conn.Close()

	// Start data transfer service
	conn.StartUnit(app.dataService, "replace", nil)
	// Run data sampling process in a timer
	conn.StartUnit(app.prService, "replace", nil)

	if g != nil {
		go g.Heartbeat(ctx, app.wdPeriod)
		// We are servicing the watchdog internally so we can stop
		// the external service.
		conn.StopUnit("diopulse.service", "replace", nil)
	}

	defer func() {
		conn.StopUnit(app.prService, "replace", nil)
		conn.StopUnit(app.dataService, "replace", nil)
	}()

	return p.WaitForEdge(gpio.Rising, -1)
}

func (app *appEnv) run(ctx0 context.Context) error {
	sw := gpio.NewPin(app.prGpio)
	sw.Out()
	g := pulse.New(app.wdGpio, app.wdWidth)
	go g.Once()

	sw.Write(gpio.High)

	time.Sleep(app.prWarmup)
	if err := app.acqSample(); err != nil {
		log.Printf("Sampling failed: %v", err)
	}

	sw.Write(gpio.Low)

	err := app.checkModem(ctx0, g)
	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}
