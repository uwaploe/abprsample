package modeminit

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/uwaploe/abprsample/internal/modem"
)

const Usage = `Usage: modeminit [options] file

Initialize the acoustic modem using the commands in the supplied file.

`

type appEnv struct {
	fl    *flag.FlagSet
	mopts []modem.Option
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("modeminit", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	amdev := fl.String("dev", "/dev/ttyAPP3", "acoustic modem serial device")
	oldModem := fl.Bool("old", false, "Use old model of acoustic modem")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing command file")
		fl.Usage()
		return flag.ErrHelp
	}

	app.mopts = []modem.Option{
		modem.ModemDevice(*amdev),
		modem.CmdFile(req[0]),
	}
	if *oldModem {
		app.mopts = append(app.mopts, modem.ATM88x(true))
	}

	return nil
}

func (app *appEnv) run(ctx0 context.Context) error {
	_, err := modem.New(app.mopts...)
	return err
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}
