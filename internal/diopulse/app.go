package diopulse

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/pulse"
)

const Usage = `Usage: diopulse [options] line

Generate a watchdog pulse sequence on a DIO line. Ctrl-c to exit.

`

type appEnv struct {
	fl       *flag.FlagSet
	wdGpio   int
	wdPeriod time.Duration
	wdWidth  time.Duration
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("modeminit", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.DurationVar(&app.wdPeriod, "wd-period", time.Second*60,
		"watchdog pulse period")
	fl.DurationVar(&app.wdWidth, "wd-width", time.Millisecond*500,
		"watchdog pulse width")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing DIO line")
		fl.Usage()
		return flag.ErrHelp
	}

	app.wdGpio, err = strconv.Atoi(req[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Invalid DIO line: %q", req[0])
		return flag.ErrHelp
	}

	return nil
}

func (app *appEnv) run(ctx0 context.Context) error {
	fmt.Fprintf(os.Stderr, "Ctrl-c to exit ... ")
	g := pulse.New(app.wdGpio, app.wdWidth)
	g.Once()
	g.Heartbeat(ctx0, app.wdPeriod)

	return nil
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}
