package amcp

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/atmmsg"
	"github.com/golang/protobuf/proto"
)

func getData(ctx context.Context, store *abprdata.DataStore,
	req *abprdata.DataReq) (int, io.Reader) {
	view := store.NewView(int(req.Minute))
	ch := view.Walk(ctx, req.Dstart(), int(req.Ndays))
	s := &abprdata.Store{}
	for rec := range ch {
		s.Records = append(s.Records, rec)
	}
	ds := s.Compress()
	b, err := proto.Marshal(ds)
	if err != nil {
		return 0, nil
	}

	return len(b), bytes.NewReader(b)
}

type DataServer struct {
	store   *abprdata.DataStore
	ackTime time.Duration
	pktSize uint
}

func NewDataServer(dir string, options ...Option) *DataServer {
	opts := serverOptions{
		ackTime: time.Second * 35,
		pktSize: 2048,
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	return &DataServer{
		store:   abprdata.NewDataStore(dir),
		ackTime: opts.ackTime,
		pktSize: opts.pktSize,
	}
}

func (d *DataServer) Reply(ctx context.Context, r *Request, rw io.ReadWriter) error {
	var err error

	switch r.Code {
	case GetData:
		req := &abprdata.DataReq{}
		proto.Unmarshal(r.Body, req)
		dsize, rdr := getData(ctx, d.store, req)
		if rdr == nil {
			return fmt.Errorf("Invalid data request: %s", req)
		}
		// The data request can optionally specify the time to
		// wait for acknowlegments and the data block-size.
		if req.Acktime > 0 {
			d.ackTime = time.Duration(req.Acktime) * time.Second
		}
		if req.Blocksize > 0 {
			d.pktSize = uint(req.Blocksize)
		}
		if debugLog != nil {
			debugLog("Data request: %s (%d bytes)", req, dsize)
		}
		time.Sleep(time.Second * 2)
		sender := atmmsg.NewSender(rw)
		sender.SetRetries(2)
		sender.SetPktSize(d.pktSize)
		err = sender.Send(ctx, rdr, d.ackTime)
	case GetDataRange:
		dr := d.store.Range()
		enc := atmmsg.NewEncoder(rw)
		buf := proto.NewBuffer([]byte{DataRangeResp})
		buf.Marshal(&dr)
		enc.Write(buf.Bytes())
		if debugLog != nil {
			debugLog("GetDataRange => %s", &dr)
		}
	}

	return err
}
