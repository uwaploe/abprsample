// Package amcp implements the "server" side of ABPR acoustic modem
// communication protocol. The logic is based on the http server
// implementation from the standard library. Handlers are registered for
// each request type received from the "client".
package amcp

import (
	"context"
	"io"
	"time"

	"bitbucket.org/uwaploe/atmmsg"
)

const (
	GetDataRange   byte = 0x41
	GetData             = 0x42
	SessionDone         = 0x43
	ExecCommand         = 0x44
	GetStatus           = 0x45
	DataRangeResp       = 0xc1
	DataResp            = 0xc2
	ExecResponse        = 0xc4
	StatusResponse      = 0xc5
)

type Logger func(string, ...interface{})

var debugLog Logger = nil

// SetDebugLogger will enable or disable logging messages
func SetDebugLogger(logger Logger) {
	debugLog = logger
	atmmsg.TraceFunc = logger
}

type Request struct {
	Code byte
	Body []byte
}

// A Handler receives a client request and sends the response (if any).
type Handler interface {
	Reply(context.Context, *Request, io.ReadWriter) error
}

// Like the http package, a function can also be used as a Handler
type HandlerFunc func(context.Context, *Request, io.ReadWriter) error

func (f HandlerFunc) Reply(ctx context.Context, r *Request, rw io.ReadWriter) error {
	return f(ctx, r, rw)
}

type ServeMux struct {
	rw         io.ReadWriter
	replyDelay time.Duration
	handlers   map[byte]Handler
}

type serverOptions struct {
	replyDelay time.Duration
	ackTime    time.Duration
	pktSize    uint
}

type Option struct {
	f func(*serverOptions)
}

func ReplyDelay(t time.Duration) Option {
	return Option{func(opts *serverOptions) {
		opts.replyDelay = t
	}}
}

func AckWait(t time.Duration) Option {
	return Option{func(opts *serverOptions) {
		opts.ackTime = t
	}}
}

func PacketSize(n uint) Option {
	return Option{func(opts *serverOptions) {
		opts.pktSize = n
	}}
}

func NewServeMux(rw io.ReadWriter, options ...Option) *ServeMux {
	opts := serverOptions{
		replyDelay: time.Second * 2,
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	return &ServeMux{
		rw:         rw,
		replyDelay: opts.replyDelay,
		handlers:   make(map[byte]Handler),
	}
}

func (m *ServeMux) Handle(reqtype byte, handler Handler) {
	m.handlers[reqtype] = handler
}

func (m *ServeMux) HandleFunc(reqtype byte, handler func(context.Context, *Request, io.ReadWriter) error) {
	m.handlers[reqtype] = HandlerFunc(handler)
}

// Serve receives client messages and dispatches them to registered handlers
func (m *ServeMux) Serve(ctx context.Context) error {
	for {
		// Wait for next packet
		payload, _, err := atmmsg.ReadRawPacketCtx(ctx, m.rw)
		if err != nil {
			return err
		}

		if payload[0] == SessionDone {
			if debugLog != nil {
				debugLog("Shutting down")
			}
			break
		}

		req := Request{Code: payload[0], Body: payload[1:]}
		if handler, ok := m.handlers[payload[0]]; ok {
			if debugLog != nil {
				debugLog("Handling request: %02x", req.Code)
			}
			time.Sleep(m.replyDelay)
			err = handler.Reply(ctx, &req, m.rw)
			if debugLog != nil && err != nil {
				debugLog("Request failed: %v", err)
			}
		} else {
			if debugLog != nil {
				debugLog("No registered handler: %02x", req.Code)
			}
		}
	}

	return nil
}
