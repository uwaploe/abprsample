package amcp

import (
	"bytes"
	"context"
	"io"
	"os/exec"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/atmmsg"
	"github.com/golang/protobuf/proto"
)

type ExecServer struct {
}

func NewExecServer() *ExecServer {
	return &ExecServer{}
}

func runCmd(ctx context.Context, req *abprdata.ExecReq) abprdata.ExecResp {
	var (
		stdout, stderr bytes.Buffer
		resp           abprdata.ExecResp
	)

	if req.Timeout > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(req.Timeout)*time.Second)
		defer cancel()
	}
	cmd := exec.CommandContext(ctx, req.Cmd[0], req.Cmd[1:]...)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if err := cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			resp.Exitcode = int32(exitError.ExitCode())
		}
	}
	resp.Stdout = stdout.Bytes()
	resp.Stderr = stderr.Bytes()

	return resp
}

func (e *ExecServer) Reply(ctx context.Context, r *Request, rw io.ReadWriter) error {
	switch r.Code {
	case ExecCommand:
		req := &abprdata.ExecReq{}
		proto.Unmarshal(r.Body, req)
		resp := runCmd(ctx, req)
		enc := atmmsg.NewEncoder(rw)
		buf := proto.NewBuffer([]byte{ExecResponse})
		buf.Marshal(&resp)
		enc.Write(buf.Bytes())
	}

	return nil
}
