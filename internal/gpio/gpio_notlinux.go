// Package gpio emulates control of GPIO pins via the Linux sysfs interface

// +build !linux

package gpio

import (
	"errors"
	"log"
	"os"
	"strconv"
	"time"
)

type GpioDir string

const (
	In  GpioDir = "in"
	Out         = "out"
)

type GpioVal int

const (
	Low  GpioVal = 0
	High         = 1
)

type GpioEdge string

const (
	Rising  GpioEdge = "rising"
	Falling          = "falling"
)

const (
	// GPIOPATH default linux gpio path
	GPIOPATH = "/sys/class/gpio"
)

var ErrNotExported = errors.New("pin has not been exported")
var ErrTimeout = errors.New("Operation timed out")

type Pin struct {
	pin       string
	label     string
	value     *os.File
	direction *os.File
	valPath   string
}

func NewPin(pin int) *Pin {
	p := &Pin{
		pin: strconv.Itoa(pin),
	}
	p.label = "gpio" + p.pin

	return p
}

func (p *Pin) String() string {
	return p.label
}

func (p *Pin) Out() error {
	if err := p.Export(); err != nil {
		return err
	}
	return p.Direction(Out)
}

func (p *Pin) In() error {
	if err := p.Export(); err != nil {
		return err
	}
	return p.Direction(In)
}

// Direction configures the pin to be input or output
func (p *Pin) Direction(dir GpioDir) error {
	_, err := writeFile(p.direction, []byte(dir))
	return err
}

// Write sets the value of an output pin
func (p *Pin) Write(val GpioVal) error {
	_, err := writeFile(p.value, []byte(strconv.Itoa(int(val))))
	log.Printf("[%s] Write %v", p.label, val)
	return err
}

// Read returns the value of an input pin
func (p *Pin) Read() (GpioVal, error) {
	buf, err := readFile(p.value)
	if err != nil {
		return 0, err
	}
	x, err := strconv.Atoi(string(buf[0]))
	return GpioVal(x), err
}

// Export allows this pin to be used for I/O
func (p *Pin) Export() error {
	return nil
}

// Unexport releases the pin
func (p *Pin) Unexport() error {
	return nil
}

// WaitForEdge waits for a pin state transition or for the timeout to expire. Set
// timeout to -1 to wait forever.
func (p *Pin) WaitForEdge(edge GpioEdge, timeout time.Duration) error {
	return nil
}

func writeFile(f *os.File, data []byte) (int, error) {
	return len(data), nil
}

func syncWriteFile(path string, data []byte) (int, error) {
	return len(data), nil
}

func readFile(f *os.File) ([]byte, error) {
	return []byte("0"), nil
}
