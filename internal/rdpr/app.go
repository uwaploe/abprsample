package rdpr

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/abprsample/internal/sci"
)

const Usage = `Usage: rdpr [options]

Read the pressure sensor and store the data record.
`

type appEnv struct {
	fl      *flag.FlagSet
	sciopts []sci.Option
	dataDir string
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("rdpr", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	prdev := fl.String("dev", "/dev/ttyAPP1", "pressure sensor serial device")
	intTime := fl.Duration("int-time", 3500*time.Millisecond, "pressure integration time")
	fl.StringVar(&app.dataDir, "data", "/data/ABPR", "data directory")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	app.sciopts = []sci.Option{
		sci.PressureDevice(*prdev),
		sci.PressureIntTime(*intTime),
		sci.NoTilt(),
	}

	return nil
}

func (app *appEnv) run(ctx0 context.Context) error {
	samp, err := sci.New(app.sciopts...)
	if err != nil {
		return err
	}
	defer samp.Close()

	if err := os.MkdirAll(app.dataDir, 0755); err != nil {
		return err
	}

	store := abprdata.NewDataStore(app.dataDir)
	defer store.Close()

	ds, err := samp.Sample()
	if err != nil {
		return err
	}
	rec := abprdata.NewRecord(ds.T, ds)

	return store.AddRecord(rec)
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}
