// Package adc provides an interface to the low-resolution A/D converter
// on the ABPR controller board.
package adc

import (
	"errors"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"strings"
)

const sysfsDir = "/sys/bus/iio/devices/iio:device0"

var ErrChannel = errors.New("Invalid channel")

type Adc struct {
	files  [16]string
	scales [16]float64
}

func readScale(channel int) (float64, error) {
	filename := filepath.Join(sysfsDir, fmt.Sprintf("in_voltage%d_scale", channel))
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, err
	}
	return strconv.ParseFloat(strings.TrimRight(string(b), " \n"), 64)
}

func NewAdc() (*Adc, error) {
	adc := &Adc{}
	for i := 0; i < 16; i++ {
		adc.files[i] = filepath.Join(sysfsDir, fmt.Sprintf("in_voltage%d_raw", i))
		x, err := readScale(i)
		if err != nil {
			adc.files[i] = ""
		} else {
			adc.scales[i] = x * 1e-3
		}
	}

	return adc, nil
}

func readRaw(path string) int {
	if path == "" {
		return 0
	}
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return 0
	}
	val, _ := strconv.Atoi(strings.TrimRight(string(b), " \n"))
	return val
}

// Voltage returns the analog voltage value for the specified A/D channel.
func (adc *Adc) Voltage(channel int) (float64, error) {
	if channel < 0 || channel > 15 {
		return 0, ErrChannel
	}
	return float64(readRaw(adc.files[channel])) * adc.scales[channel], nil
}
