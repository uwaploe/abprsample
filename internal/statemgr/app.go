package statemgr

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/gpio"
	"bitbucket.org/uwaploe/abprsample/internal/modem"
	"github.com/coreos/go-systemd/v22/dbus"
)

const Usage = `Usage: statemgr [options]

Manage the operational state for the ABPR controller. Enables the Systemd timer
based pressure sampling when an acoustic modem session is active.

`

type appEnv struct {
	fl          *flag.FlagSet
	deGpio      int
	comGpio     int
	prService   string
	dataService string
	minWait     time.Duration
	mopts       []modem.Option
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("rdpr", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	fl.IntVar(&app.deGpio, "de-gpio", 55, "GPIO line for modem Device Enable")
	fl.IntVar(&app.comGpio, "com-gpio", 53, "GPIO to enable modem communications")
	fl.StringVar(&app.prService, "svc", "rdpr.timer",
		"Systemd timer service to sample pressure")
	fl.StringVar(&app.dataService, "data-svc", "datasvc.service",
		"Systemd service to manage data transfer")
	fl.DurationVar(&app.minWait, "wait", time.Second*60,
		"minimum time to wait for modem DE")
	amdev := fl.String("dev", "/dev/ttyAPP3", "acoustic modem serial device")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	app.mopts = []modem.Option{
		modem.ModemDevice(*amdev),
	}

	return nil
}

func modemSleep(ctx context.Context, opts []modem.Option) {
	m, err := modem.New(opts...)
	if err != nil {
		return
	}
	m.SendCmds(ctx, "@IdleTimer=00:00:40", "cfg store")
	m.Lowpower(ctx)
	m.Close()
}

func modemKeepAwake(ctx context.Context, opts []modem.Option) {
	m, err := modem.New(opts...)
	if err != nil {
		return
	}
	m.SendCmds(ctx, "@IdleTimer=00:05:00", "cfg store")
	m.Lowpower(ctx)
	m.Close()
}

func (app *appEnv) run(ctx0 context.Context) error {
	p := gpio.NewPin(app.deGpio)
	if err := p.In(); err != nil {
		return err
	}

	val, err := p.Read()
	if err != nil {
		return err
	}

	// When the modem DE is asserted, the GPIO line is pulled low
	if val == gpio.High {
		// Wait a bit for a falling edge
		if err := p.WaitForEdge(gpio.Falling, app.minWait); err != nil {
			fmt.Println("DE not asserted, exiting")
			return nil
		}
	}

	comEnable := gpio.NewPin(app.comGpio)
	comEnable.Out()
	comEnable.Write(gpio.High)
	defer comEnable.Write(gpio.Low)

	ctx, cancel := context.WithTimeout(ctx0, time.Second*12)
	defer cancel()
	modemKeepAwake(ctx, app.mopts)

	conn, err := dbus.NewSystemdConnection()
	if err != nil {
		return err
	}
	defer conn.Close()

	// Start data transfer service
	conn.StartUnit(app.dataService, "replace", nil)
	// Run data sampling process in a timer
	conn.StartUnit(app.prService, "replace", nil)

	defer func() {
		conn.StopUnit(app.prService, "replace", nil)
		conn.StopUnit(app.dataService, "replace", nil)
	}()

	return p.WaitForEdge(gpio.Rising, -1)
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}

	return 0
}
