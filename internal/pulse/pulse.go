// Package pulse generates a low-high-low pulse on a DIO line
package pulse

import (
	"context"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/gpio"
)

type Generator struct {
	pin   *gpio.Pin
	width time.Duration
}

func New(line int, width time.Duration) *Generator {
	g := &Generator{
		pin:   gpio.NewPin(line),
		width: width,
	}
	g.pin.Out()
	g.pin.Write(gpio.Low)

	return g
}

func (g *Generator) Once() {
	g.pin.Write(gpio.High)
	time.Sleep(g.width)
	g.pin.Write(gpio.Low)
}

func (g *Generator) Heartbeat(ctx context.Context, period time.Duration) {
	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(period):
			g.Once()
		}
	}
}
