package sci

import (
	"strings"
	"time"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"bitbucket.org/mfkenney/paros"
)

// Implement the abprdata.StatusSample interface
type DataSample struct {
	T    time.Time
	p    paros.Measurement
	tilt map[string]float32
}

func NewSample(t time.Time, p paros.Measurement, vals []tcm.DataValue) DataSample {
	rec := make(map[string]float32)
	for _, val := range vals {
		name := strings.ToLower(val.Id.String())
		rec[name], _ = val.Data.(float32)
	}
	return DataSample{T: t, p: p, tilt: rec}
}

// Pressure in meters of seawater
func (ds DataSample) Pr() float64 {
	return ds.p.Pr * 0.67
}

func (ds DataSample) Temp() float64 {
	return ds.p.Temp
}

func (ds DataSample) Pitch() float32 {
	return ds.tilt["pangle"]
}

func (ds DataSample) Roll() float32 {
	return ds.tilt["rangle"]
}
