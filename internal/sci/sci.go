// Package sci manages the ABPR science sensors
package sci

import (
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"bitbucket.org/mfkenney/paros"
	"bitbucket.org/uwaploe/abprsample/internal/port"
)

type Sampler struct {
	pr      *paros.Device
	tilt    *tcm.Tcm
	closers []io.Closer
}

type samplerOptions struct {
	prDevice   string
	prBaud     int
	prAddr     int
	intTime    time.Duration
	tiltDevice string
	tiltBaud   int
	tiltMount  string
}

type Option struct {
	f func(*samplerOptions)
}

// Set the pressure sensor serial device
func PressureDevice(name string) Option {
	return Option{func(opts *samplerOptions) {
		opts.prDevice = name
	}}
}

// Set the pressure sensor baud rate
func PressureBaud(baud int) Option {
	return Option{func(opts *samplerOptions) {
		opts.prBaud = baud
	}}
}

// Set pressure integration time
func PressureIntTime(t time.Duration) Option {
	return Option{func(opts *samplerOptions) {
		opts.intTime = t
	}}
}

func NoTilt() Option {
	return Option{func(opts *samplerOptions) {
		opts.tiltDevice = ""
	}}
}

// Set the tilt sensor serial device
func TiltDevice(name string) Option {
	return Option{func(opts *samplerOptions) {
		opts.tiltDevice = name
	}}
}

// Set the tilt sensor baud rate
func TiltBaud(baud int) Option {
	return Option{func(opts *samplerOptions) {
		opts.tiltBaud = baud
	}}
}

// Set the tilt sensor orientation
func TiltOrientation(val string) Option {
	return Option{func(opts *samplerOptions) {
		opts.tiltMount = val
	}}
}

func initializeTcm(dev *tcm.Tcm, fields []string, ref string) error {
	err := dev.SetDataComponents(tcm.ComponentIds(fields...)...)
	if err != nil {
		return fmt.Errorf("SetDataComponents: %w", err)
	}
	mref, err := tcm.MountingRefValue(ref)
	if err != nil {
		return fmt.Errorf("Invalid mounting ref %q: %w", ref, err)
	}
	err = dev.SetConfig(tcm.Kmountingref, mref)
	if err != nil {
		return fmt.Errorf("SetConfig: %w", err)
	}
	err = dev.SetAcqParams(tcm.Polled, true, 0, 0)
	if err != nil {
		return fmt.Errorf("SetAcqParams: %w", err)
	}
	err = dev.Save()
	if err != nil {
		return fmt.Errorf("Save: %w", err)
	}
	return nil
}

func New(options ...Option) (*Sampler, error) {
	opts := samplerOptions{
		prDevice: "/dev/ttyAPP1",
		prBaud:   9600,
		prAddr:   1,
		// 70ms gives a pressure resolution of 10ppm
		intTime:    time.Millisecond * 70,
		tiltDevice: "/dev/ttyAPP2",
		tiltBaud:   38400,
		tiltMount:  "std0",
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	s := &Sampler{}
	s.closers = make([]io.Closer, 0)

	var (
		p   port.Port
		err error
	)

	if strings.Contains(opts.prDevice, ":") {
		p, err = port.NetworkPort(opts.prDevice, opts.intTime+time.Second*3)
	} else {
		p, err = port.SerialPort(opts.prDevice, opts.prBaud, opts.intTime+time.Second*3)
	}

	if err != nil {
		return nil, fmt.Errorf("Cannot open serial port %q: %w",
			opts.prDevice, err)
	}
	s.closers = append(s.closers, p)

	s.pr = paros.NewDevice(p, opts.prAddr)
	s.pr.SetIntTime(paros.Pressure, opts.intTime)
	s.pr.SetIntTime(paros.Temperature, opts.intTime)
	// Enable simultaneous integration
	s.pr.SendCmd("EW", "OI=0")

	if opts.tiltDevice != "" {
		p, err = port.SerialPort(opts.tiltDevice, opts.tiltBaud, time.Second*5)
		if err != nil {
			return nil, fmt.Errorf("Cannot open serial port %q: %w",
				opts.tiltDevice, err)
		}
		s.closers = append(s.closers, p)

		s.tilt = tcm.NewTcm(p)
		err = initializeTcm(s.tilt, []string{"pangle", "rangle"}, opts.tiltMount)
		if err != nil {
			s.Close()
			return nil, err
		}
	}

	return s, nil
}

func (s *Sampler) Close() error {
	if s.closers == nil {
		return errors.New("Ports already closed")
	}

	for _, c := range s.closers {
		c.Close()
	}
	s.closers = nil
	return nil
}

func (s *Sampler) Sample() (DataSample, error) {
	t := time.Now()
	meas, err := s.pr.Sample()
	if err != nil {
		return DataSample{}, fmt.Errorf("paros: %w", err)
	}

	var vals []tcm.DataValue

	if s.tilt != nil {
		vals, err = s.tilt.GetData()
		if err != nil {
			return DataSample{}, fmt.Errorf("TCM: %w", err)
		}
	}

	return NewSample(t, meas, vals), nil
}
