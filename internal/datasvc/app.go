package datasvc

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/amcp"
	"bitbucket.org/uwaploe/abprsample/internal/modem"
)

const Usage = `Usage: datasvc [options] datadir

`

type appEnv struct {
	fl      *flag.FlagSet
	dir     string
	mopts   []modem.Option
	svcopts []amcp.Option
	debug   bool
	semFile string
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("datasvc", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	amdev := fl.String("am-dev", "/dev/ttyAPP3", "acoustic modem serial device")
	cmdFile := fl.String("cmd", "", "acoustic modem command file")
	ackTime := fl.Duration("ack", time.Second*60, "time to wait for ACKs")
	pktSize := fl.Uint("pkt", 1024, "data packet size")
	oldModem := fl.Bool("old", false, "Use old model of acoustic modem")
	fl.BoolVar(&app.debug, "debug", false, "enable diagnostic output")
	fl.StringVar(&app.semFile, "sem", "", "semaphore file created when a connection is active")
	app.fl = fl

	if err := fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing directory name\n")
		fl.Usage()
		return flag.ErrHelp
	}
	app.dir = req[0]

	app.mopts = []modem.Option{
		modem.ModemDevice(*amdev),
		modem.CmdFile(*cmdFile),
	}
	if *oldModem {
		app.mopts = append(app.mopts, modem.ATM88x(true))
	}

	app.svcopts = []amcp.Option{
		amcp.AckWait(*ackTime),
		amcp.PacketSize(*pktSize),
	}
	return nil
}

func (app *appEnv) run(ctx context.Context) error {
	if app.debug {
		modem.SetDebugLogger(log.Printf)
		amcp.SetDebugLogger(log.Printf)
	}

	am, err := modem.New(app.mopts...)
	if err != nil {
		return err
	}

	conn, err := am.Listen(ctx)
	if err != nil {
		return err
	}

	if app.semFile != "" {
		ioutil.WriteFile(app.semFile, []byte{}, 0644)
		defer os.Remove(app.semFile)
	}

	mux := amcp.NewServeMux(conn)
	svc := amcp.NewDataServer(app.dir, app.svcopts...)

	mux.Handle(amcp.GetData, svc)
	mux.Handle(amcp.GetDataRange, svc)
	mux.Handle(amcp.ExecCommand, amcp.NewExecServer())

	return mux.Serve(ctx)
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}
	return 0
}
