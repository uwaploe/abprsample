// Package modem controls the Bethos acoustic modem
package modem

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/port"
	"bitbucket.org/uwaploe/atm"
)

type Logger func(string, ...interface{})

var debugLog Logger = nil

// SetDebugLogger will enable or disable logging messages
func SetDebugLogger(logger Logger) {
	debugLog = logger
	atm.TraceFunc = logger
}

type Modem struct {
	dev *atm.Device
	p   port.Port
}

type modemOptions struct {
	devname      string
	baud         int
	rdTimeout    time.Duration
	setupTimeout time.Duration
	cmdFile      string
	isAtm88x     bool
}

type Option struct {
	f func(*modemOptions)
}

// Modem serial device
func ModemDevice(name string) Option {
	return Option{func(opts *modemOptions) {
		opts.devname = name
	}}
}

// Modem baud rate
func ModemBaud(baud int) Option {
	return Option{func(opts *modemOptions) {
		opts.baud = baud
	}}
}

func ATM88x(state bool) Option {
	return Option{func(opts *modemOptions) {
		opts.isAtm88x = state
	}}
}

// File containing modem commands
func CmdFile(path string) Option {
	return Option{func(opts *modemOptions) {
		opts.cmdFile = path
	}}
}

func modemSetup(ctx0 context.Context, dev *atm.Device, rdr io.Reader) error {
	ctx, cancel := context.WithTimeout(ctx0, time.Second*3)
	_, err := dev.Exec(ctx, "AT")
	cancel()

	if err != nil {
		ctx, cancel = context.WithTimeout(ctx0, time.Second*3)
		err = dev.Interrupt(ctx)
		cancel()
		if err != nil {
			return fmt.Errorf("No response from modem: %w", err)
		}
	}

	if rdr != nil {
		_, err = dev.Batch(ctx0, rdr)
	}

	return err
}

// Return a new Modem using the supplied options
func New(options ...Option) (*Modem, error) {
	opts := modemOptions{
		devname:      "/dev/ttyAPP3",
		baud:         9600,
		rdTimeout:    time.Millisecond * 2500,
		setupTimeout: time.Second * 20,
	}

	for _, opt := range options {
		opt.f(&opts)
	}

	var (
		p   port.Port
		err error
	)

	if strings.Contains(opts.devname, ":") {
		p, err = port.NetworkPort(opts.devname, opts.rdTimeout)
	} else {
		p, err = port.SerialPort(opts.devname, opts.baud, opts.rdTimeout)
	}

	if err != nil {
		return nil, fmt.Errorf("Cannot access modem at %q: %w", opts.devname, err)
	}

	atmOpts := make([]atm.Option, 0)
	if opts.isAtm88x {
		atmOpts = append(atmOpts, atm.ATM88x())
	}

	dev := atm.NewDevice(p, atmOpts...)

	var rdr io.Reader
	if opts.cmdFile != "" {
		f, err := os.Open(opts.cmdFile)
		if err != nil {
			return nil, fmt.Errorf("Cannot open command file %q: %v", opts.cmdFile, err)
		}
		defer f.Close()
		rdr = f
		ctx, cancel := context.WithTimeout(context.Background(), opts.setupTimeout)
		defer cancel()
		modemSetup(ctx, dev, rdr)
		err = dev.Lowpower(ctx)
	}

	return &Modem{dev: dev, p: p}, err
}

// Listen waits for a connection from a remote modem
func (m *Modem) Listen(ctx context.Context) (atm.Conn, error) {
	return m.dev.GoOnline(ctx)
}

// Lowpower forces the modem into low power mode
func (m *Modem) Lowpower(ctx context.Context) error {
	modemSetup(ctx, m.dev, nil)
	return m.dev.Lowpower(ctx)
}

func (m *Modem) SendCmds(ctx context.Context, cmds ...string) error {
	if err := modemSetup(ctx, m.dev, nil); err != nil {
		return err
	}

	for _, cmd := range cmds {
		if _, err := m.dev.Exec(ctx, cmd); err != nil {
			return err
		}
	}

	return nil
}

func (m *Modem) Close() {
	m.p.Close()
}
