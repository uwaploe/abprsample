package deployment

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"bitbucket.org/uwaploe/abprsample/internal/amcp"
	"bitbucket.org/uwaploe/abprsample/internal/ema"
	"bitbucket.org/uwaploe/abprsample/internal/gpio"
	"bitbucket.org/uwaploe/abprsample/internal/modem"
	"bitbucket.org/uwaploe/abprsample/internal/pulse"
	"bitbucket.org/uwaploe/abprsample/internal/sci"
	"bitbucket.org/uwaploe/atmmsg"
	"github.com/golang/protobuf/proto"
)

const Usage = `Usage: deployment [options] timeout

This program runs during the ABPR deployment phase and responds to status
requests over the acoustic modem link. The timeout argument is the
maximum run time.

`

type sysState struct {
	sci.DataSample
	vspeed float64
}

func (s sysState) Speed() float64 {
	return s.vspeed
}

type appEnv struct {
	fl             *flag.FlagSet
	samp           *sci.Sampler
	sciopts        []sci.Option
	mopts          []modem.Option
	timeout        time.Duration
	samples        chan sysState
	allowInterrupt bool
	debug          bool
	comGpio        int
	wdGpio         int
	wdPeriod       time.Duration
	wdWidth        time.Duration
}

var versReq = errors.New("show version")

func (app *appEnv) fromArgs(args []string) error {
	fl := flag.NewFlagSet("deployment", flag.ContinueOnError)
	fl.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		fl.PrintDefaults()
	}

	showVers := fl.Bool("version", false, "Show program version information and exit")
	prdev := fl.String("pr-dev", "/dev/ttyAPP1", "pressure sensor serial device")
	tiltdev := fl.String("tilt-dev", "/dev/ttyAPP2", "tilt sensor serial device")
	tiltref := fl.String("ref", "std0", "TCM mounting reference value")
	amdev := fl.String("am-dev", "/dev/ttyAPP3", "acoustic modem serial device")
	cmdFile := fl.String("cmd", "", "acoustic modem command file")
	fl.BoolVar(&app.debug, "debug", false, "enable diagnostic output")
	fl.BoolVar(&app.allowInterrupt, "int", false,
		"allow deployment mode to be interrupted via modem command")
	fl.IntVar(&app.comGpio, "com-gpio", 53, "GPIO to enable modem communications")
	fl.IntVar(&app.wdGpio, "wd-gpio", 52, "GPIO to signal watchdog")
	fl.DurationVar(&app.wdPeriod, "wd-period", time.Second*60,
		"watchdog pulse period")
	fl.DurationVar(&app.wdWidth, "wd-width", time.Millisecond*500,
		"watchdog pulse width")
	app.fl = fl

	var err error
	if err = fl.Parse(args); err != nil {
		return err
	}

	if *showVers {
		return versReq
	}

	req := fl.Args()
	if len(req) == 0 {
		fmt.Fprintf(os.Stderr, "Missing timeout value")
		fl.Usage()
		return flag.ErrHelp
	}

	if app.timeout, err = time.ParseDuration(req[0]); err != nil {
		return fmt.Errorf("Invalid duration value, %q: %w", req[0], err)
	}

	if *tiltdev == "" || *tiltdev == "/dev/null" {
		app.sciopts = []sci.Option{
			sci.PressureDevice(*prdev),
			sci.NoTilt(),
		}
	} else {
		app.sciopts = []sci.Option{
			sci.PressureDevice(*prdev),
			sci.TiltDevice(*tiltdev),
			sci.TiltOrientation(*tiltref),
		}
	}

	app.mopts = []modem.Option{
		modem.ModemDevice(*amdev),
		modem.CmdFile(*cmdFile),
	}

	return nil
}

// Sample the pressure and tilt sensors at a constant rate and calculate the
// vertical speed. The current sysState is written to the channel
// app.samples.
func (app *appEnv) monitor(ctx context.Context,
	interval time.Duration) error {
	var (
		s0, s1 sci.DataSample
		state  sysState
		err    error
	)

	s0, err = app.samp.Sample()
	if err != nil {
		return err
	}
	// Moving average filter for the speed estimate
	filter := ema.New(0.18)
	state.DataSample = s0

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			s1, err = app.samp.Sample()
			if err != nil {
				return err
			}
			state.DataSample = s1
			dt := s1.T.Sub(s0.T)
			state.vspeed = filter.Update((state.Pr() - s0.Pr()) / dt.Seconds())
			s0 = s1
			select {
			case app.samples <- state:
			default:
			}
		}
	}
}

// Reply implements the amcp.Handler interface
func (app *appEnv) Reply(ctx context.Context, req *amcp.Request, rw io.ReadWriter) error {
	var state sysState
	select {
	case state = <-app.samples:
	case <-time.After(time.Second * 5):
		return errors.New("Sample timeout")
	}
	status := abprdata.NewStatus(state.DataSample.T, state)
	enc := atmmsg.NewEncoder(rw)
	buf := proto.NewBuffer([]byte{amcp.StatusResponse})
	buf.Marshal(status)
	enc.Write(buf.Bytes())
	return nil
}

func (app *appEnv) run(ctx0 context.Context) error {
	if app.debug {
		modem.SetDebugLogger(log.Printf)
		amcp.SetDebugLogger(log.Printf)
	}

	samp, err := sci.New(app.sciopts...)
	if err != nil {
		return err
	}
	app.samp = samp
	app.samples = make(chan sysState, 1)
	defer close(app.samples)

	// Enable modem serial communications
	comEnable := gpio.NewPin(app.comGpio)
	comEnable.Out()
	comEnable.Write(gpio.High)
	defer comEnable.Write(gpio.Low)

	am, err := modem.New(app.mopts...)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(ctx0, app.timeout)
	defer cancel()

	conn, err := am.Listen(ctx)
	if err != nil {
		return err
	}

	go app.monitor(ctx, time.Second)

	// Start watchdog pulse generator
	if app.wdGpio > 0 {
		g := pulse.New(app.wdGpio, app.wdWidth)
		go g.Heartbeat(ctx, app.wdPeriod)
	}

	mux := amcp.NewServeMux(conn)
	mux.Handle(amcp.GetStatus, app)
	mux.Handle(amcp.ExecCommand, amcp.NewExecServer())
	if app.allowInterrupt {
		mux.HandleFunc(amcp.SessionDone,
			func(ctx context.Context, r *amcp.Request, rw io.ReadWriter) error {
				log.Println("Ending deployment session")
				cancel()
				return nil
			})
	}

	return mux.Serve(ctx)
}

func CLI(ctx context.Context, vers string, args []string) int {
	var app appEnv
	err := app.fromArgs(args)
	if err != nil {
		switch err {
		case versReq:
			fmt.Fprintf(os.Stderr, "%s\n", vers)
		}

		return 2
	}

	if err = app.run(ctx); err != nil && err != context.DeadlineExceeded {
		log.Printf("Runtime error: %v\n", err)
		return 1
	}

	return 0
}
