// Package power switches power to an ABPR peripheral device.
package power

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/gpio"
)

type Switch interface {
	On() error
	Off() error
	Release()
}

type FET struct {
	pin         *gpio.Pin
	initialized bool
}

func pinFromName(name string) (int, error) {
	if strings.HasPrefix(strings.ToLower(name), "gpio") {
		return strconv.Atoi(name[4:])
	}

	return strconv.Atoi(name)
}

// NewFET returns a new FET
func NewFET(name string) (*FET, error) {
	f := FET{}
	num, err := pinFromName(name)
	if err != nil {
		return nil, err
	}
	f.pin = gpio.NewPin(num)
	return &f, nil
}

func (f *FET) String() string {
	return fmt.Sprintf("FET(%s)", f.pin)
}

func (f *FET) Release() {
	f.pin.Unexport()
}

func (f *FET) On() error {
	if !f.initialized {
		f.initialized = true
		f.pin.Out()
	}
	return f.pin.Write(gpio.High)
}

func (f *FET) Off() error {
	if !f.initialized {
		f.initialized = true
		f.pin.Out()
	}
	return f.pin.Write(gpio.Low)
}

type Relay struct {
	on_pin, off_pin *gpio.Pin
	delay           time.Duration
	initialized     bool
}

// NewRelay returns a new relay with an on pin and and off pin along
// with a switching duration.
func NewRelay(on, off string, swtime time.Duration) (*Relay, error) {
	r := Relay{delay: swtime}

	num, err := pinFromName(on)
	if err != nil {
		return nil, err
	}
	r.on_pin = gpio.NewPin(num)

	num, err = pinFromName(off)
	if err != nil {
		return nil, err
	}
	r.off_pin = gpio.NewPin(num)

	return &r, nil
}

func (r *Relay) String() string {
	return fmt.Sprintf("Relay(on=%s, off=%s, delay=%s)",
		r.on_pin, r.off_pin, r.delay)
}

func (r *Relay) Release() {
	r.on_pin.Unexport()
	r.off_pin.Unexport()
}

// On switches the Relay on
func (r *Relay) On() error {
	if !r.initialized {
		r.on_pin.Out()
		r.off_pin.Out()
		r.initialized = true
	}
	err := r.on_pin.Write(gpio.High)
	if err != nil {
		return err
	}
	time.Sleep(r.delay)
	return r.on_pin.Write(gpio.Low)
}

// Off switches the Relay off
func (r *Relay) Off() error {
	if !r.initialized {
		r.on_pin.Out()
		r.off_pin.Out()
		r.initialized = true
	}
	err := r.off_pin.Write(gpio.High)
	if err != nil {
		return err
	}
	time.Sleep(r.delay)
	return r.off_pin.Write(gpio.Low)
}
