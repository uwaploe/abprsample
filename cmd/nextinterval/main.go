// Nextinterval halts the system until the next sampling interval. The system
// is powered off immediately.
package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"
)

const Usage = `Usage: nextinterval [options] interval

Use the supervisory microcontroller to shutdown the system and restart it at
the next sample time. Interval can be expressed as a duration or as an integer
number of seconds.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dryRun bool
)

const tsMicroCtl = "/usr/local/bin/tsmicroctl"

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.BoolVar(&dryRun, "n", dryRun, "Print the shutdown command but don't execute it")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func secsToSleep(t0 time.Time, interval time.Duration) int64 {
	t := t0.Unix()
	secs := int64(interval / time.Second)
	next := (1 + t/secs) * secs
	return next - t
}

func main() {
	args := parseCmdLine()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		interval time.Duration
		err      error
	)

	if interval, err = time.ParseDuration(args[0]); err != nil {
		if secs, err := strconv.ParseUint(args[0], 10, 32); err != nil {
			fmt.Fprintf(os.Stderr, "Invalid interval: %q", args[0])
			os.Exit(2)
		} else {
			interval = time.Second * time.Duration(secs)
		}
	}

	secs := secsToSleep(time.Now(), interval)
	cmdline := []string{
		tsMicroCtl,
		"--sleep",
		"--timewkup",
		fmt.Sprintf("%d", secs),
		"--resetswitchwkup",
	}

	if dryRun {
		fmt.Printf("%s\n", cmdline)
	} else {
		cmd := exec.Command(cmdline[0], cmdline[1:]...)
		err := cmd.Run()
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v", err)
			os.Exit(3)
		}
	}
}
