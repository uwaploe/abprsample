package main

import (
	"testing"
	"time"
)

func TestInterval(t *testing.T) {
	table := []struct {
		t0  time.Time
		dt  time.Duration
		out int64
	}{
		{
			t0:  time.Date(1970, time.January, 1, 12, 15, 0, 0, time.UTC),
			dt:  15 * time.Minute,
			out: 900,
		},
		{
			t0:  time.Date(1970, time.January, 1, 12, 29, 30, 0, time.UTC),
			dt:  15 * time.Minute,
			out: 30,
		},
		{
			t0:  time.Date(1970, time.January, 1, 12, 16, 0, 0, time.UTC),
			dt:  15 * time.Minute,
			out: 840,
		},
		{
			t0:  time.Date(1970, time.January, 1, 12, 15, 0, 1234, time.UTC),
			dt:  15 * time.Minute,
			out: 900,
		},
	}

	for _, e := range table {
		out := secsToSleep(e.t0, e.dt)
		if out != e.out {
			t.Errorf("Bad result; expected %v, got %v", e.out, out)
		}
	}
}
