// Powerctl switches the power to one or more ABPR subsystems
package main

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/power"
	toml "github.com/pelletier/go-toml"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func configureSwitch(name string, tree *toml.Tree) (power.Switch, error) {
	if tree.HasPath([]string{"power", name, "on"}) {
		on := tree.GetPath([]string{"power", name, "on"}).(string)
		off := tree.GetPath([]string{"power", name, "off"}).(string)
		delay := tree.GetPath([]string{"power", name, "delay"}).(string)
		if on == "" || off == "" || delay == "" {
			return nil, fmt.Errorf("Invalid relay configuration for %q", name)
		}
		dt, err := time.ParseDuration(delay)
		if err != nil {
			return nil, fmt.Errorf("Invalid time duration: %q", delay)
		}
		return power.NewRelay(on, off, dt)
	}

	sw, ok := tree.GetPath([]string{"power", name}).(string)
	if !ok || sw == "" {
		return nil, fmt.Errorf("Invalid switch configuration for %q", name)
	}

	return power.NewFET(sw)
}

func main() {
	app := cli.NewApp()
	app.Name = "powerctl"
	app.Usage = "Switch power to ABPR subsystems"
	app.Version = Version

	home, _ := os.UserHomeDir()

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "cfg",
			Usage:  "Subsystem configuration file",
			Value:  filepath.Join(home, "config", "abpr.toml"),
			EnvVar: "ABPR_CFG",
		},
		cli.BoolFlag{
			Name:  "debug",
			Usage: "Enable debug output",
		},
		cli.VersionFlag,
	}

	var tree *toml.Tree
	subsys := []string{"paros", "tilt", "modem", "spare", "spare5v"}
	switches := make(map[string]power.Switch)

	app.Before = func(c *cli.Context) error {
		var err error
		tree, err = toml.LoadFile(c.String("cfg"))
		if err != nil {
			return cli.NewExitError(fmt.Sprintf("Cannot read config file: %v", err), 1)
		}
		for _, name := range subsys {
			sw, err := configureSwitch(name, tree)
			if err != nil {
				continue
			}
			switches[name] = sw
		}
		if c.GlobalBool("debug") {
			for _, name := range subsys {
				fmt.Fprintf(os.Stderr, "%s: %s\n", name, switches[name])
			}
		}
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:      "on",
			Usage:     "power-on one or more subsystems",
			ArgsUsage: "SUBSYS...",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				for _, name := range c.Args() {
					if sw, ok := switches[name]; !ok {
						return cli.NewExitError(fmt.Sprintf("invalid name: %q", name), 2)
					} else {
						if err := sw.On(); err != nil {
							return err
						}
					}
				}

				return nil
			},
		},
		{
			Name:      "off",
			Usage:     "power-off one or more subsystems",
			ArgsUsage: "SUBSYS...",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				for _, name := range c.Args() {
					if sw, ok := switches[name]; !ok {
						return cli.NewExitError(fmt.Sprintf("invalid name: %q", name), 2)
					} else {
						if err := sw.Off(); err != nil {
							return nil
						}
					}
				}

				return nil
			},
		},
		{
			Name:  "list",
			Usage: "list subsystems",
			Action: func(c *cli.Context) error {
				for name := range switches {
					fmt.Println(name)
				}
				return nil
			},
		},
	}

	app.Run(os.Args)
}
