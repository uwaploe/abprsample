// Diowait waits for an input line to change state
package main

import (
	"os"
	"strconv"

	"bitbucket.org/uwaploe/abprsample/internal/gpio"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	app := cli.NewApp()
	app.Name = "diowait"
	app.Usage = "Wait for a digital input state change"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.DurationFlag{
			Name:  "t",
			Usage: "Maximum time to wait, -1 waits forever",
			Value: -1,
		},
		cli.VersionFlag,
	}

	app.Commands = []cli.Command{
		{
			Name:      "rising",
			Usage:     "wait for a rising edge",
			ArgsUsage: "PIN",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				n, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					return cli.NewExitError("bad PIN value", 1)
				}
				pin := gpio.NewPin(n)
				if err := pin.In(); err != nil {
					return cli.NewExitError(err.Error(), 2)
				}

				if err := pin.WaitForEdge(gpio.Rising, c.GlobalDuration("t")); err != nil {
					return cli.NewExitError(err.Error(), 2)
				}

				return nil
			},
		},
		{
			Name:      "falling",
			Usage:     "wait for a falling edge",
			ArgsUsage: "PIN",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				n, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					return cli.NewExitError("bad PIN value", 1)
				}
				pin := gpio.NewPin(n)
				if err := pin.In(); err != nil {
					return cli.NewExitError(err.Error(), 2)
				}

				if err := pin.WaitForEdge(gpio.Falling, c.GlobalDuration("t")); err != nil {
					return cli.NewExitError(err.Error(), 2)
				}

				return nil
			},
		},
	}

	app.Run(os.Args)
}
