// Sensread provides a CLI to read the ABPR science sensors
package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/sci"
	tea "github.com/charmbracelet/bubbletea"
)

const Usage = `Usage: sensread [options]

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	sampInterval time.Duration = time.Second
	inLine       bool
	tiltRef      string = "std0"
)

type tickMsg time.Time

type model struct {
	interval time.Duration
	sampler  *sci.Sampler
	ds       sci.DataSample
	err      error
}

func tick(interval time.Duration) tea.Cmd {
	return tea.Tick(interval, func(t time.Time) tea.Msg {
		return tickMsg(t)
	})
}

func (m model) Init() tea.Cmd {
	return tick(m.interval)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		}
	case tickMsg:
		m.ds, m.err = m.sampler.Sample()
		return m, tick(m.interval)
	}

	return m, nil
}

const TimeFormat = "2006-01-02T15:04:05.000Z07:00"

func (m model) View() string {
	s := "ABPR Sensors\n\n"
	s += fmt.Sprintf("Pressure=%.3f m\n", m.ds.Pr())
	s += fmt.Sprintf("Temperature=%.2f °C\n", m.ds.Temp())
	s += fmt.Sprintf("Pitch=%.2f °\n", m.ds.Pitch())
	s += fmt.Sprintf("Roll=%.2f °\n", m.ds.Roll())
	s += fmt.Sprintf("%s\n", time.Now().Format(TimeFormat))
	s += fmt.Sprintln("(ctrl-c to quit)")
	if m.err != nil {
		s += fmt.Sprintf("ERROR: %v\n", m.err)
	}
	return s
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&sampInterval, "i", sampInterval,
		"sample interval")
	flag.BoolVar(&inLine, "inline", inLine, "display output inline")
	flag.StringVar(&tiltRef, "ref", tiltRef, "tilt sensor orientation")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	_ = parseCmdLine()

	s, err := sci.New(sci.PressureIntTime(time.Millisecond*70),
		sci.TiltOrientation(tiltRef))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Cannot access sensors: %v", err)
		os.Exit(1)
	}

	m := model{
		interval: sampInterval,
		sampler:  s,
	}
	p := tea.NewProgram(m)

	if !inLine {
		p.EnterAltScreen()
		defer p.ExitAltScreen()
	}

	if p.Start() != nil {
		fmt.Fprintln(os.Stderr, "Cannot start program")
		return
	}
}
