// Adread provides a CLI to read the ABPR A/D channels
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/abprsample/internal/adc"
	"github.com/BurntSushi/toml"
	tea "github.com/charmbracelet/bubbletea"
)

const Usage = `Usage: adread [options]

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	sampInterval time.Duration = time.Second
	cfgFile      string
	inLine       bool
)

type adChan struct {
	Chan  int       `toml:"chan"`
	Name  string    `toml:"name"`
	Units string    `toml:"units"`
	Coeff []float64 `toml:"coeff"`
}

func (a adChan) Scale(x float64) float64 {
	return a.Coeff[0] + a.Coeff[1]*x
}

func defaultChan(i int) adChan {
	return adChan{
		Chan:  i,
		Name:  fmt.Sprintf("ADC[%d]", i),
		Units: "V",
		Coeff: []float64{0, 1},
	}
}

type tickMsg time.Time

type model struct {
	interval time.Duration
	input    *adc.Adc
	channels []int
	vals     map[int]float64
	meta     map[int]adChan
}

func tick(interval time.Duration) tea.Cmd {
	return tea.Tick(interval, func(t time.Time) tea.Msg {
		return tickMsg(t)
	})
}

func (m model) Init() tea.Cmd {
	return tick(m.interval)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		}
	case tickMsg:
		for _, channel := range m.channels {
			m.vals[channel], _ = m.input.Voltage(channel)
		}
		return m, tick(m.interval)
	}

	return m, nil
}

const TimeFormat = "2006-01-02T15:04:05.000Z07:00"

func (m model) View() string {
	s := "ABPR A/D Inputs\n\n"
	for _, channel := range m.channels {
		meta := m.meta[channel]
		s += fmt.Sprintf("%s=%.4f %s\n", meta.Name,
			meta.Scale(m.vals[channel]),
			meta.Units)
	}
	s += fmt.Sprintf("%s\n", time.Now().Format(TimeFormat))
	return s
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.DurationVar(&sampInterval, "i", sampInterval,
		"ADC sample interval")
	flag.StringVar(&cfgFile, "cfg", cfgFile, "A/D channel configuration file")
	flag.BoolVar(&inLine, "inline", inLine, "display output inline")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

type config struct {
	Chans []adChan `toml:"channel"`
}

func main() {
	_ = parseCmdLine()

	input, err := adc.NewAdc()
	if err != nil {
		log.Fatal(err)
	}

	var cfg config
	if cfgFile != "" {
		contents, err := ioutil.ReadFile(cfgFile)
		if err != nil {
			log.Fatalf("Config file error: %v", err)
		}
		_, err = toml.Decode(string(contents), &cfg)
		if err != nil {
			log.Fatalf("Config error: %v", err)
		}
	} else {
		cfg.Chans = make([]adChan, 3)
		for i := 0; i < 3; i++ {
			cfg.Chans[i] = defaultChan(i + 1)
		}
	}

	m := model{
		interval: sampInterval,
		input:    input,
		vals:     make(map[int]float64),
		meta:     make(map[int]adChan),
	}

	m.channels = make([]int, 0)
	for _, c := range cfg.Chans {
		m.channels = append(m.channels, c.Chan)
		m.meta[c.Chan] = c
	}

	p := tea.NewProgram(m)
	if !inLine {
		p.EnterAltScreen()
		defer p.ExitAltScreen()
	}

	if p.Start() != nil {
		fmt.Fprintln(os.Stderr, "Cannot start program")
		return
	}
}
