// ABPR sampling state management
package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	app "bitbucket.org/uwaploe/abprsample/internal/statemgr"
)

var Version = "dev"
var BuildDate = "unknown"

func main() {
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Cancel the context on a signal interrupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
			os.Exit(0)
		}
	}()

	os.Exit(app.CLI(ctx, Version, os.Args[1:]))
}
