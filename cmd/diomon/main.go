// Diomon monitors the state of an input line
package main

import (
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"bitbucket.org/uwaploe/abprsample/internal/gpio"
	"github.com/urfave/cli"
)

var Version = "dev"
var BuildDate = "unknown"

func run(c *cli.Context) error {
	if c.NArg() < 1 {
		return cli.NewExitError("missing arguments", 1)
	}
	n, err := strconv.Atoi(c.Args().Get(0))
	if err != nil {
		return cli.NewExitError("bad PIN value", 1)
	}
	pin := gpio.NewPin(n)
	if err := pin.In(); err != nil {
		return cli.NewExitError(err.Error(), 2)
	}

	for {
		val, err := pin.Read()
		if err != nil {
			return cli.NewExitError(err.Error(), 2)
		}
		switch val {
		case gpio.High:
			log.Println("Input HIGH")
			err = pin.WaitForEdge(gpio.Falling, -1)
			if err != nil {
				return cli.NewExitError(err.Error(), 2)
			}
			log.Println("Falling edge")
		case gpio.Low:
			log.Println("Input LOW")
			err = pin.WaitForEdge(gpio.Rising, -1)
			if err != nil {
				return cli.NewExitError(err.Error(), 2)
			}
			log.Println("Rising edge")
		}
	}

	return nil
}

func main() {
	app := cli.NewApp()
	app.Name = "diomon"
	app.Usage = "Monitor digital input state changes"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.VersionFlag,
	}

	app.Action = run

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Cancel the context on a signal interrupt
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			os.Exit(1)
		}
	}()

	app.Run(os.Args)
}
