#!/usr/bin/env bash
#
# Run the ABPR deployment mode
#
export PATH=$PATH:/usr/local/bin

: ${POWER_CFG=/home/sysop/config/abpr.toml}
: ${TILT_REF="xup0"}
: ${DEPLOYMENT_TIME="4h"}
: ${SAMPLE_INTERVAL=15m}

t="$1"
[[ -z $t ]] && t=$DEPLOYMENT_TIME

powerctl --cfg $POWER_CFG on paros tilt
deployment --int --ref $TILT_REF $t
# Sync the filesystem and halt the OS until the next sample interval
/bin/sync
/bin/sync
nextinterval $SAMPLE_INTERVAL
