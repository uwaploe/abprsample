#!/bin/bash
#
# Run an ABPR data collection sequence
#
export PATH=$PATH:/usr/local/bin

: ${PAROS_WARMUP=0}
: ${MODEM_WAIT=0}
: ${SAMPLE_INTERVAL=15m}

trap "exit 0" HUP INT TERM
datasample --data /data/ABPR --wait $MODEM_WAIT

# delay system halt while the sysop user is logged-in
while loginctl show-user sysop > /dev/null 2>&1; do
    sleep 1
done
# systemctl -q disable rdpr.timer
# systemctl -q stop rdpr.timer
# [[ $(systemctl is-enabled nextinterval.service) = "enabled" ]] && systemctl halt
/bin/sync
/bin/sync
nextinterval $SAMPLE_INTERVAL
