#!/usr/bin/env bash
#
# Menu UI for ABPR setup
#

: ${CFGDIR=/home/sysop/config}

check_sampling ()
{
    [[ $(systemctl is-enabled sample.service 2>/dev/null) = "enabled" ]]
}

enable_sampling ()
{
    systemctl -q enable sample.service
    systemctl -q disable powerinit.service diopulse.service
}

disable_sampling ()
{
    dialog --infobox "Shutting down sampling service ..." 8 40 2> /dev/null
    systemctl -q stop sample.service
    systemctl -q disable sample.service
    systemctl -q enable powerinit.service diopulse.service
    systemctl -q start powerinit.service diopulse.service
}

deploy ()
{
    local dt hours
    [[ -f $CFGDIR/abpr.env ]] && source $CFGDIR/abpr.env
    dt="${DEPLOYMENT_TIME:-4h}"
    hours=$(dialog --inputbox "Deployment time limit (hours)" 10 40 "${dt/h}" 2>&1 >/dev/tty)
    dt="${hours}h"
    if [[ "$dt" != "$DEPLOYMENT_TIME" ]]; then
        echo "DEPLOYMENT_TIME=$dt" >> $CFGDIR/abpr.env
    fi
    dialog --infobox "Starting deployment ..." 8 40 2> /dev/null
    systemctl -q stop datasvc.service rdpr.service rdpr.timer diopulse.service
    systemctl -q disable rdpr.timer diopulse.service
    systemctl daemon-reload
    systemctl restart deployment.service
    enable_sampling
}

modem_passthru ()
{
    local cmdfile="$(mktemp)"
    cat<<EOF > "$cmdfile"
set line /dev/ttyAPP3
set speed 9600
set flow none
set carrier off
set duplex half
connect
EOF
    kermit "$cmdfile"
    rm -f "$cmdfile"
}

testmenu ()
{
    local options=("rs" "Read Sensors" \
                   "deploy" "Test Deployment" \
                   "data" "Test Data Transmission" \
                   "pass" "Modem Pass-through" \
                   "q" "Return")
    local done choice secs t0
    done=
    while [[ -z $done ]]; do
        choice=$(dialog --title "ABPR Test" \
                        --no-tags \
                        --no-ok \
                        --no-cancel \
                        --menu "Choose one and press Enter:" 0 0 10 "${options[@]}" 2>&1 >/dev/tty)
        case "$choice" in
            rs)
                powerctl --cfg $CFGDIR/abpr.toml on paros tilt
                sensread --ref xup0
                powerctl --cfg $CFGDIR/abpr.toml off paros tilt
                ;;
            pass)
                systemctl -q stop deployment.service datasvc.service
                # Enable modem serial comms
                powerctl --cfg $CFGDIR/abpr.toml on modemrx
                dialog --msgbox "ctrl-\ q exits session" 8 40 2> /dev/null
                modem_passthru
                powerctl --cfg $CFGDIR/abpr.toml off modemrx
                ;;
            deploy)
                systemctl -q stop deployment.service datasvc.service
                secs=$(dialog --inputbox "Deployment time limit (seconds)" 10 40 300 2>&1 >/dev/tty)
                if [[ -n $secs ]]; then
                    systemctl -q stop diopulse.service
                    powerctl --cfg $CFGDIR/abpr.toml on paros tilt
                    t0="$(date +'%F %T')"
                    export JOURNAL_STREAM=1
                    systemd-cat -t "deployment-test" deployment --debug --int --ref xup0 "${secs}s" &
                    pid=$!
                    if dialog --no-ok --no-cancel --pause "Waiting for deployment mode to end" 8 40 "$secs" 2> /dev/null; then
                        wait $pid
                    else
                        kill -TERM $pid
                    fi
                    powerctl --cfg $CFGDIR/abpr.toml off paros tilt
                    systemctl start diopulse.service
                    # Save log
                    mkdir -p $HOME/logs
                    journalctl --since="$t0" -t deployment-test > $HOME/logs/deployment-test.log
                fi
                ;;
            data)
                systemctl -q stop deployment.service datasvc.service
                t0="$(date +'%F %T')"
                export JOURNAL_STREAM=1
                systemd-cat -t "datasvc-test" datasvc --debug --am-dev /dev/ttyAPP3 $HOME/TESTDATA &
                pid=$!
                dialog --msgbox "Data service running. Press Enter to stop" 8 40 2> /dev/null
                kill -TERM $pid
                # Save log
                mkdir -p $HOME/logs
                journalctl --since="$t0" -t datasvc-test > $HOME/logs/datasvc-test.log
                ;;
            q)
                done="yes"
                ;;
        esac
    done
}

options=("tm" "Test System >" \
         "im" "Initialize Modem" \
         "rc" "Read Clock" \
         "sc" "Set Clock" \
         "samp" "Enable Sampling" \
         "dis" "Disable Sampling" \
         "deploy" "Deploy" \
         "cancel" "Cancel deployment" \
         "q" "QUIT")

done=
while [[ -z $done ]]; do
    if check_sampling; then
        state="sampling enabled"
    else
        state="sampling disabled"
    fi

    choice=$(dialog --title "ABPR Setup ($state)" \
                    --no-tags \
                    --no-cancel \
                    --no-ok \
                    --menu "Choose one and press Enter:" 0 0 10 "${options[@]}" 2>&1 >/dev/tty)
    case "$choice" in
        tm)
            testmenu
            ;;
        im)
            # Enable modem serial comms
            powerctl --cfg $CFGDIR/abpr.toml on modemrx
            file=$CFGDIR/atm-setup.txt
            [[ -s $file ]] && {
                dialog --infobox "Initializing modem ..." 8 40 2> /dev/null
                if systemctl -q is-active datasvc.service; then
                    systemctl -q stop datasvc.service
                fi
                modeminit $file
            }
            powerctl --cfg $CFGDIR/abpr.toml off modemrx
            ;;
        samp)
            enable_sampling
            dialog --msgbox "Sampling will start on next reboot" 8 40 2> /dev/null
            ;;
        dis)
            disable_sampling
            ;;
        deploy)
            deploy
            ;;
        cancel)
            if systemctl -q is-active deployment.service; then
                dialog --infobox "Deployment service stopped" 8 40 2> /dev/null
                systemctl -q stop deployment.service
                systemctl start diopulse.service
            fi
            ;;
        rc)
            dialog --msgbox "$(date +'%F %T')" 8 40 2> /dev/null
            ;;
        sc)
            t0="$(date +%s)"
            # Round to the next minute
            tnext=$(((1 + t0/60)*60))
            t="$(dialog --inputbox "Enter UTC time as YYYY-mm-dd HH:MM:SS" \
                              10 40 "$(date -d @$tnext +'%F %T')" 2>&1 >/dev/tty)"
            if [[ -n $t ]]; then
                date --set="$t" -u
                /sbin/hwclock --systohc -u
                dialog --msgbox "Clock updated: $(date +'%F %T')" 8 40 2> /dev/null
            fi
            ;;
        q)
            done="yes"
            ;;
    esac
done

clear
