# ABPR Data Acquisition

This repository contains the source code for the ABPR data acquistion software, see the [software architecture document](architecture.md) for the details.

## Installation

Download the most recent Debian package from the Downloads section of this repository and install on the ABPR host system using `dpkg`.

``` shellsession
$ dpkg -i abprsample_VERSION_arm.deb
```
