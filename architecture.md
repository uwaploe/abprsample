# ABPR Sampling

This document describes the high-level architecture of the ABPR data sampling and storage software contained in this repository. All of the programs are written in [Go](https://golang.org).

## Overview

The ABPR host computer boots-up every 15 minutes, acquires and stores a pressure sample, checks for an acoustic modem communication request, and shuts down. The tasks are handled by a set of Systemd services.

### sample.service

This service runs the script `abprsample.sh` which runs the `datasample` program then halts the system until the next sample interval.

#### datasample

This program runs the data sampling sequence:

  - Pulse the external watchdog circuit
  - Power on the pressure sensor
  - Acquire and store a sample
  - Power off the pressure sensor
  - Check the modem device enable (DE) line to determine if a connection has been requested. If it has, start `datasvc.service` to manage the data download and `rdpr.timer` to continue sampling the pressure every 15 minutes. It also starts a thread to send a heartbeat pulse to the external watchdog circuit. See the flow chart below.

![Flowchart](statemgr_flowchart.svg "statemgr flow chart")

### datasvc.service

This service runs the `datasvc` program which listens for data download requests over the acoustic modem.

### nextinterval.service

This service runs during the system halt sequence. It syncs the file-system then runs the vendor supplied `tshwctl` program to power down the CPU and restart it at the next 15-minute sample interval.

### diopulse.service

This service provides a "heartbeat" signal for the external watchdog circuit. The service is started late in the boot process so it does not run when sampling is active.

## Software Organization

- `bin/` -- Shell (Bash) scripts
- `cmd/` -- Main program source
- `internal/` -- Go package source (libraries)
- `scripts/` -- Debian package scripts, i.e. "maintainer scripts"
- `systemd/` -- Systemd unit files

## Building and installing

[Goreleaser](https://goreleaser.com) is required to build the software and create a Debian Linux package.

``` shellsession
$ goreleaser release --skip-validate --skip-publish --rm-dist
```

Debian package is stored under `./dist`.
