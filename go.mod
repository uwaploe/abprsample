module bitbucket.org/uwaploe/abprsample

go 1.13

require (
	bitbucket.org/mfkenney/go-tcm v1.3.0
	bitbucket.org/mfkenney/paros v0.2.0
	bitbucket.org/uwaploe/abprdata v0.9.10
	bitbucket.org/uwaploe/atm v0.8.1
	bitbucket.org/uwaploe/atmmsg v0.6.3
	github.com/BurntSushi/toml v0.3.1
	github.com/charmbracelet/bubbletea v0.12.4
	github.com/coreos/go-systemd/v22 v22.3.1
	github.com/golang/protobuf v1.5.2
	github.com/pelletier/go-toml v1.9.0
	github.com/spf13/afero v1.6.0 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	github.com/urfave/cli v1.22.5
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a
	golang.org/x/text v0.3.6 // indirect
)

// replace bitbucket.org/uwaploe/abprdata => ../pkg/abprdata

// replace bitbucket.org/uwaploe/atmmsg => ../pkg/atmmsg

// replace bitbucket.org/uwaploe/atm => ../../Link16/atm
